import os
import sys

### BEGIN SETTINGS ###
from django.conf import settings

DEBUG = os.environ.get('DEBUG', 'on') == 'on'

SECRET_KEY = os.environ.get('SECRET_KEY', os.urandom(32))

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

MEDIA_ROOT = os.path.join(BASE_DIR, 'files')

settings.configure(
    DEBUG=DEBUG,
    SECRET_KEY=SECRET_KEY,
    BASE_DIR=BASE_DIR,
    ROOT_URLCONF='wbk.urls',
    MEDIA_ROOT=MEDIA_ROOT,
    MIDDLEWARE_CLASSES=(
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ),
    INSTALLED_APPS=(
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.staticfiles',
        'haystack',
        'wbk',
    ),
    STATIC_URL='/static/',
    DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'wbk.db',
            }
    },
    HAYSTACK_CONNECTIONS = {
            'default': {
                'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
                'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
            },
    },
)
### END SETTINGS ###

### START WSGI ###
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
### END WSGI ###

### START MANAGER ###
if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

### END MANAGER ###
