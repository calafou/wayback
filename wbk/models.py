from django.conf import settings
from django.db import models

import os


class List(models.Model):
    """
    It collects the strictly necessary information of a mailing list.
    """

    name = models.CharField(max_length=256, unique=True)


class User(models.Model):
    """
    It collects the strictly necessary information of a mailing list user.
    """

    email_address = models.EmailField(unique=True)


class ListUser(models.Model):
    """
    It represents the paired-relationship between a user 
    and a list. In this way we can easily identify a specific 
    user of a specific list without having to create a many2many 
    relationship between User and List.
    """

    m_list = models.ForeignKey(List)
    user = models.ForeignKey(User)


class EmailMessage(models.Model):
    """
    It collects the necessary information of an email message.
    """

    to_recipient = models.ForeignKey(List)
    from_recipient = models.ForeignKey(ListUser)
    subject = models.CharField(max_length=256)
    body = models.TextField(max_length=102400)
    date = models.DateTimeField()
    message_id = models.CharField(max_length=256, unique=True)
    content_type = models.CharField(max_length=256)
    user_agent = models.CharField(max_length=256)
    reference = models.CharField(max_length=1024)


class MediaObject(models.Model):
    """
    It collects the strictly neccessary information of media 
    objects. Media objects make possible correlation between 
    a date in the list history and a media.
    """

    TYPE_VIDEO = 'video'
    TYPE_AUDIO = 'audio'
    TYPE_PIC = 'pic'

    TYPE_CHOICES = (
        (TYPE_VIDEO, 'Video'),
        (TYPE_AUDIO, 'Audio'),
        (TYPE_PIC, 'Picture'),
    )
    c_type = models.CharField(max_length=128, choices=TYPE_CHOICES)
    url_link = models.URLField(unique=True)
    m_list = models.ForeignKey(List)


class EmailAttachment(models.Model):
    """
    It preserves an email attachment
    """
    message = models.ForeignKey(EmailMessage)
    attachment = models.FileField(upload_to=os.path.join(settings.MEDIA_ROOT, 'mail_attachments'))



    
    


    
