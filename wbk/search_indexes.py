from haystack import indexes

from wbk.models import EmailMessage


class EmailMessageIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    to_recip = indexes.CharField(model_attr='to_recipient__name')
    from_recip = indexes.CharField(model_attr='from_recipient__user__email_address')
    subject = indexes.CharField(model_attr='subject')
    body = indexes.CharField(model_attr='body')
    date = indexes.DateTimeField(model_attr='date')
    m_id = indexes.CharField(model_attr='message_id')
    c_type = indexes.CharField(model_attr='content_type')
    u_agent = indexes.CharField(model_attr='user_agent')
    references = indexes.CharField(model_attr='reference')

    def get_model(self):
        return EmailMessage

    def index_queryset(self, using=None):
        return self.get_model().objects.all()



    
