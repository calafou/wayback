from django.conf import settings
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from wbk.models import List, User, ListUser, EmailMessage, MediaObject, \
    EmailAttachment

from datetime import datetime
import email
import os
import re


def _get_email_from_header(header):
    name, address = email.utils.parseaddr(header)
    return address.lower()
    

class Command(BaseCommand):
    help = 'Import a mailing list archive. Atm only supports archive made by 1file per email message'

    def handle(self, *args, **options):
        SUCCESSES = 0
        FAILURES = 0
        
        archives_dir = os.path.join(settings.BASE_DIR, 'archives')
        
        email_messages_paths = [os.path.join(dirpath, name) \
                                for (dirpath, _, filenames) in os.walk(archives_dir) \
                                for name in filenames if filenames]

        TOTAL = len(email_messages_paths)
        
        for message in email_messages_paths:
            with open(message, encoding="ISO-8859-1") as fp:
                msg = email.message_from_file(fp)

            try:
                user_email = _get_email_from_header(msg.get("From"))
                mailing_list = _get_email_from_header(msg.get("Delivered-To"))
                subject = msg.get("Subject")
                date_tuple = email.utils.parsedate_tz(msg.get("Date"))
                date = datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
                message_id = msg.get("Message-ID")
                content_type = msg.get_content_type()
                user_agent = msg.get("User-Agent", '')
                reference = msg.get("References", '')
            except Exception as e:
                self.stderr.write("[%s/%s] Impossible to parse email message: %s because: %s" % (str(SUCCESSES+FAILURES), str(TOTAL), message, str(e)))
                FAILURES += 1
                continue

            with transaction.atomic():
                try:
                    list_obj, list_created = List.objects.get_or_create(name=mailing_list)
                    user_obj, user_created = User.objects.get_or_create(email_address=user_email)
                    # Create ListUser if necessary or get it
                    if list_created or user_created:
                        list_user = ListUser.objects.create(
                            m_list=list_obj,
                            user=user_obj,
                        )
                    else:
                        list_user = ListUser.objects.get(
                            m_list=list_obj,
                            user=user_obj,
                        )

                    # Let's create a base email message to fulfill
                    email_msg = EmailMessage(
                        to_recipient=list_obj,
                        from_recipient=list_user,
                        subject=subject,
                        date=date,
                        message_id=message_id,
                        content_type=content_type,
                        reference=reference,
                        user_agent=user_agent,
                    )

                    for part in msg.walk():
                        c_type = part.get_content_type()
                        c_disp = part.get('Content-Disposition')
                
                        if c_type == 'text/plain' and not c_disp:
                            email_msg.body += part.get_payload() + '\n'

                    email_msg.save()
                    
                    attachment_counter = 0
                    for part in msg.walk():
                        if c_disp and ('attachment' in c_disp or 'inline' in c_disp):
                            try:
                                f_name, ext = os.path.splitext(part.get_filename())
                            except:
                                f_name = None
                                ext = None
                                
                            c_type = part.get_content_type()
                            
                            if f_name and ext:
                                attachment_counter += 1
                                name = "attachment-%s-%s%s" % (str(SUCCESSES+FAILURES), str(attachment_counter), ext)
                                content = part.get_payload(decode=True)
                                
                                file_to_upload = SimpleUploadedFile(
                                    name=name,
                                    content=content,
                                    content_type=c_type
                                )
                                attachment = EmailAttachment.objects.create(
                                    message=email_msg,
                                    attachment=file_to_upload,
                                )
                                
                    SUCCESSES += 1
                    self.stdout.write("[%s/%s] Message successfully created from: %s" % (str(SUCCESSES+FAILURES), str(TOTAL), message))
                except Exception as e:
                    FAILURES += 1
                    self.stderr.write("[%s/%s] The following errors cause to fail the message at path %s: %s" % (str(SUCCESSES+FAILURES), str(TOTAL), message, str(e)))
                    continue

        self.stdout.write("%s successfully imported" % str(SUCCESSES))
        self.stdout.write("%s left unimported" % str(FAILURES))

        
