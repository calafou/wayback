from django.shortcuts import render


def home(request):
    """
    Home page
    """
    title = "Home"

    return render(
        request,
        "home.html",
        {
            "title": title,
        },
    )

