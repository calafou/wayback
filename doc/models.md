# Relational models


![Relational models](img/relational_models.png)


## List


### Description

It collects the strictly necessary information of a mailing list.


### Attributes

* name
* description


## User


### Description

It collects the strictly necessary information of a mailing list user.


### Attributes

* email_address


## ListUser


### Description

It represents the paired-relationship between a user and a list. In this way we can easily identify a specific user of a specific list without having to create a many2many relationship between User and List.


### Attribtutes

* list -> ForeignKey(List)
* user -> ForeignKey(User)


## EmailMessage


### Description

It collects the necessary information of an email message.


### Attributes


* to -> ForeignKey(List)
* from -> ForeignKey(ListUser)
* subject
* body
* date
* message_id
* content_type
* user_agent
* reference


## MediaObject


### Description

It collects the strictly neccessary information of media objects. Media objects make possible correlation between a date in the list history and a media.


### Attributes

* type
* link
* list -> ForeignKey(List)


## Email Attachment


### Description

It preserves an email attachment


### Attributes

* message -> ForeignKey(EmailMessage)
* attachment


