# CONTRIBUTING.md

The following project adheres to [Semantic versioning](http://semver.org)


## Set up the development environment (debian testing - 2016-07-03)

### System requirements

```
sudo apt install git python3 python3-dev pyhton3-venv python3-pip
```


### Create dedicated user

```
$ sudo adduser wbk
$ sudo su - wbk
```


### Generate a ssh key

```
$ ssh-keygen

# After creation succeeded, copy the contents of /home/wbk/.ssh/id_rsa.pub in the gitlab web interface at SSH-Keys section
```


### Clone the repo

```
$ git clone git@gitlab.com:calafou/wayback.git
$ cd wayback
```


### Create a virtual environment

```
$ pyvenv wayback-env
$ source wayback-env/bin/acitvate
# Automatically activate the environment
$ echo "source $VIRTUAL_ENV/bin/activate" >> ~/.bashrc
```


### Install project requirements

```
$ pip3 install -r requirements.txt
$ pip3 install git+https://github.com/django-haystack/django-haystack.git
```


### Apply migrations

```
$ python3 wayback.py migrate
```


### Import archive

Extract your archive in `archives/` directory and then:

```
python3 wayback.py import_archive
```

or

```
python3 wayback.py import_archive &> import.log
```

or

```
python3 wayback.py import_archive 2> error.log 1> import.log
```


### Build indexes 

```
python3 wayback.py rebuild_index
```

### Run dev server

```
python3 wayback.py runserver
```




