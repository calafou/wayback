# README.md

## Wayback

Wayback it is a mailing-lists' archive funky browser. It is written in leightweight Django and Python3, powered by Xapian, embellished by Bootsrap and d3.js, run by Gunicorn.

At the moment in inception development stage.


## How to setup a development environment

Please follow guidelines at [CONTRIBUTING.md](CONTRIBUTING.md)


## How to install (debian stretch - 2016-07-03)

### System requirements

```
sudo apt install git python3 python3-dev pyhton3-venv python3-gunicorn python3-pip
```


### Clone the repo

```
$ git clone https://gitlab.com/calafou/wayback.git
```


### Create a virtual environment

```
$ cd wayback
$ pyvenv wayback-env
$ source wayback-env/bin/activate
# Automatically load the environment (optional)
$ echo "source $VIRTUAL_ENV/bin/activate" >> ~/.bashrc
```


### Install project requirements

```
$ pip3 install -r requirements.txt
$ pip3 install git+https://github.com/django-haystack/django-haystack.git

```


### Apply migrations

```
$ python3 wayback.py migrate
```


### Import archive

Extract your archive in `archives/` directory and then:

```
python3 wayback.py import_archive
```

or

```
python3 wayback.py import_archive &> import.log
```

or

```
python3 wayback.py import_archive 2> error.log 1> import.log
```


### Build indexes 

```
python3 wayback.py rebuild_index
```

